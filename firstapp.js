const http = require('http');

const requestListener =(request,response) =>{
    response.write('Hello World');
    response.end();

    //  is similar to the below line of code.

    // response.end('Hello World\n');
}

const server = http.createServer(requestListener);

const confirmationCallBack =()=>{
    console.log('Server is running....');
}


server.listen(9000,confirmationCallBack);
       
